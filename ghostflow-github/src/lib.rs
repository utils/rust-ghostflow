// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

mod authorization;

// Required to be in the root for `graphql-client`.
mod client;
pub(crate) mod queries;
pub use crate::client::Github;
pub use crate::client::GithubError;
pub use crate::queries::RateLimitInfo;

mod ghostflow;
pub use crate::ghostflow::GithubService;
