// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::env;
use std::fs;
use std::os;

use git_workarea::GitContext;
use tempfile::TempDir;

use crate::tests::log;

pub fn test_workspace_dir() -> TempDir {
    log::setup_logging();

    let mut working_dir = env::current_exe().unwrap();
    working_dir.pop();

    TempDir::new_in(working_dir).unwrap()
}

pub fn check_git_config_value(ctx: &GitContext, name: &str, expected: &str) {
    let value = get_git_config(ctx, name);
    let actual = value.trim();

    assert_eq!(actual, expected);
}

pub fn check_git_config_values(ctx: &GitContext, name: &str, expected: &[&str]) {
    let value = get_git_config(ctx, name);
    let actual = value.lines().collect::<Vec<_>>();

    assert_eq!(actual.len(), expected.len());
    assert_eq!(actual, expected);
}

fn get_git_config(ctx: &GitContext, name: &str) -> String {
    let config = ctx
        .git()
        .arg("config")
        .arg("--get-all")
        .arg(name)
        .output()
        .unwrap();

    String::from_utf8_lossy(&config.stdout).into_owned()
}

pub fn install_hook(hook: &str, name: &str, ctx: &GitContext) {
    let config = ctx
        .git()
        .arg("config")
        // XXX(git-2.46.0): use `git config set`
        // .arg("set")
        // .arg("--type=bool")
        .arg("receive.advertisePushOptions")
        .arg("true")
        .output()
        .unwrap();
    if !config.status.success() {
        panic!(
            "failed to set `receive.advertisePushOptions`: {}",
            String::from_utf8_lossy(&config.stderr),
        );
    }

    let origin = format!("{}/test/{}-{}", env!("CARGO_MANIFEST_DIR"), hook, name);
    let link = ctx.gitdir().join("hooks").join(hook);

    #[cfg(unix)]
    if os::unix::fs::symlink(&origin, &link).is_ok() {
        return;
    }

    fs::copy(origin, link).unwrap();
}
