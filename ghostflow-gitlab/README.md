# ghostflow-gitlab

This crate implements the host traits for the [GitLab][] hosting service.

[GitLab]: https://gitlab.com
